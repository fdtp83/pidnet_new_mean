import os
import glob
import random

# read from images directory
images_path = glob.glob('./images/*.png')

# split path and filename
images_path = [os.path.split(f) for f in images_path]

# get only the filename
images = [f[1] for f in images_path]

# make random selection    
random.seed(42)
random.shuffle(images)


num_val_images = 2500
# write first X images to the file val.lst
with open('../list/gtav/val.lst', 'w') as filehandle:
    for index in range(num_val_images):
        string = f'images/{images[index]}\tlabels/{images[index]}\n'
        filehandle.write(string)

# write next X images to the file test.lst
with open('../list/gtav/test.lst', 'w') as filehandle:
    for index in range(num_val_images, num_val_images*2):
        string = f'images/{images[index]}\tlabels/{images[index]}\n'
        filehandle.write(string)

# write rest to the file train.lst
with open('../list/gtav/train.lst', 'w') as filehandle:
    for index in range(num_val_images*2, len(images)):
        string = f'images/{images[index]}\tlabels/{images[index]}\n'
        filehandle.write(string)