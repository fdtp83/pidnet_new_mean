clear;
clc;

%labelFilenames = dir('..\..\Cityscapes\gtFine_trainvaltest\gtFine\train\aachen\*labelIds.png');
labelFilenames = dir('..\output\labels\*.png');

max_total = 10;
min_total = 10;

for i=1:length(labelFilenames)
%for i=1:1000
    disp(i)
    labelFilename = strcat(labelFilenames(i).folder, '\', labelFilenames(i).name);
    labelFile = imread(labelFilename);
    min_ = min(unique(labelFile));
    max_ = max(unique(labelFile));

    if(max_ > max_total)
        max_total = max_;
    end

    if(min_ < min_total)
        min_total = min_;
    end
end

disp(min_total)
disp(max_total)