clear;
clc;

out_dir = '../output/labels';

% read all label filenames
labelFilenames = dir('../data/labels/*.png');

%parfor i=1:length(labelFilenames)
for i=1:1
    disp(i)
    labelFilename = strcat(labelFilenames(i).folder, '\', labelFilenames(i).name);
    outFilename = strcat(out_dir, '\', labelFilenames(i).name);
    [labels, currentMapping] = imread(labelFilename);
    labels = labels -1;
    imwrite(labels, outFilename)
end

disp('finished')