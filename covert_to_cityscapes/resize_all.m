clear;
clc;

mapping = load('mapping.mat', 'cityscapesMap', 'camvidMap', 'classes');
out_dir = '../resize/';

% read all image filenames
imageFilenames = dir('../data/images/*.png');
labelFilenames = dir('../output/labels/*.png');

% Input Image: 1914 x 1052
% Upscale auf: 2048 x 1052*2048/1914 um nicht zu verzerren
% -> 1125,7 px
% Dann Zuschneiden auf 2048*1024

parfor i=1:length(labelFilenames)
%parfor i=1:5000
%for i=1:1
    disp(i)
    labelFilename = strcat(labelFilenames(i).folder, '\', labelFilenames(i).name);
    imageFilename = strcat(imageFilenames(i).folder, '\', imageFilenames(i).name);
    outlabelFilename = strcat(out_dir, 'labels/', labelFilenames(i).name);
    outimageFilename = strcat(out_dir, 'images/', imageFilenames(i).name);
    labelFile = imread(labelFilename);
    imageFile = imread(imageFilename);
    labelFileResize = imresize(labelFile,[1125 2048], 'nearest');
    imageFileResize = imresize(imageFile,[1125 2048], 'nearest');
    labelFileCrop = imcrop(labelFileResize, [0,  0, 2048, 1024]);
    imageFileCrop = imcrop(imageFileResize, [0,  0, 2048, 1024]);
    imwrite(labelFileCrop, outlabelFilename);
    imwrite(imageFileCrop, outimageFilename);
end

disp('finished')