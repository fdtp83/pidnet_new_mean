# Training

## Benötigte Python-Pakete

PyTorch muss über conda installiert werden.

Restliche Pakete über pip:

```bash
pip install -r requirements.txt
```

## Symlink to the dataset

### Windows

In admin PowerShell:

```powershell
# Cityscapes

New-Item -Path .\Python\pidnet\data\cityscapes\gtFine -ItemType SymbolicLink -Value C:\Users\Daniel\Documents\SoftwareProjects\ki\datasets\Cityscapes\gtFine_trainvaltest\gtFine
New-Item -Path .\Python\pidnet\data\cityscapes\leftImg8bit -ItemType SymbolicLink -Value C:\Users\Daniel\Documents\SoftwareProjects\ki\datasets\Cityscapes\leftImg8bit_trainvaltest\leftImg8bit

# GTAV
New-Item -Path .\data\gtav\images -ItemType SymbolicLink -Value C:\Users\Daniel\Documents\SoftwareProjects\ki\datasets\GTAV-Dataset\resize\images
New-Item -Path .\data\gtav\labels -ItemType SymbolicLink -Value C:\Users\Daniel\Documents\SoftwareProjects\ki\datasets\GTAV-Dataset\resize\labels

# GTAV resize
New-Item -Path .\data\gtavresize\images -ItemType SymbolicLink -Value C:\Users\Daniel\Documents\SoftwareProjects\ki\datasets\GTAV-Dataset\resize\images
New-Item -Path .\data\gtavresize\labels -ItemType SymbolicLink -Value C:\Users\Daniel\Documents\SoftwareProjects\ki\datasets\GTAV-Dataset\resize\labels
```

### Linux

```bash
ln -s /run/media/daniel/Win11/Users/Daniel/Documents/SoftwareProjects/ki/datasets/Cityscapes/gtFine_trainvaltest/gtFine/ ./Python/pidnet/data/cityscapes/gtFine 
ln -s /run/media/daniel/Win11/Users/Daniel/Documents/SoftwareProjects/ki/datasets/Cityscapes/leftImg8bit_trainvaltest/leftImg8bit/ ./Python/pidnet/data/cityscapes/leftImg8bit
```
