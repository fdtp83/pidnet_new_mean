# Deep Learning für Computer Vision - Gruppe 5

## Ausgewähltes Thema

**PROJECT #A3: REAL-TIME SEMANTIC SEGMENTATION**

Real-time semantic segmentation is the task of achieving computationally efficient semantic segmentation (while maintaining a base level of accuracy).

https://paperswithcode.com/task/real-time-semantic-segmentation

## Anforderungen

### Projekttyp A

Paper with code: taking a task from the pool with available paper (algorithms), code and dataset to:

* Option 1: Develop a new deep learning technique to solve the problem
* Option 2: Reimplement of one of top three papers listed for that problem on a new dataset (transfer learning)
  
  Note: In Option 1 if your new DL technique can get into top 3, you and your team members will get grade A (sehr gut 1.0)

### Allgemeine Anforderungen

* The aim of the final project is to prepare you to apply Deep Learning (DL) methods to real-world computer vision tasks!
* The final project should be perfomred in a team of 2 to 4 students
* Each team is permitted to select only one project from the pool
* The teams are allowed to work on the same project topic with different datasets
* The project topics are announced on 28.11.2022 (Kick-off 05.12.2022; one week time to set your topic)
* The project should be implemented and uploaded into THM-GitLab

* The project should consist of:
  * DL Model (Software)
  * Documentation (as a paper format with minimum 8 pages)
  * Evaluation metrics and results
  * Testing Framework (Inference, Training)
  * Team Präsentation (30 min)

* Grading:
  * Results of your model /software based on defined metrics (60%)
  * Documnetation as report (paper format) and poster (20%)
  * Presentation and answering the questions (20%)

* Project review and bugfixing: 16.01.2023
* Deadline & Presentation: 23.01.2023

## Implementierung

### Framework

Als Framework für die Implementierung des Projekts wird TensorFlow 2.x verwendet.

### Datasets

Als Datensatz wird das Cityscapes Dataset verwendet.

### Model

Als Model wird PIDNet verwendet.

### Evaluation

Als Metrik wird der Mean Intersection over Union (mIoU) verwendet.

## Dokumentation

### Paper