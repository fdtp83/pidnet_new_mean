# ------------------------------------------------------------------------------
# Modified based on https://github.com/HRNet/HRNet-Semantic-Segmentation
# ------------------------------------------------------------------------------

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .cityscapes import Cityscapes as cityscapes
from .camvid import CamVid as camvid
from .gtav import Gtav as gtav
from .gtavresize import Gtavresize as gtavresize
from .cgtavresize import Cgtavresize as cgtavresize
