import os
import glob

images = glob.glob('./images/*.png')
labels = glob.glob('./labels/*.png')

images = [f for f in images]
labels = [f for f in labels]

if(len(images) != len(labels)):
    print('Images and labels are not equal')
    exit()

# write first 500 to the file val.lst
with open('../list/gtavresize/val.lst', 'w') as filehandle:
    for index in range(500):
        string = f'{images[index]}\t{labels[index]}\n'
        string = string.replace('\\', '/')
        string = string.replace('./', '')
        filehandle.write(string)

# write rest to the file train.lst
with open('../list/gtavresize/train.lst', 'w') as filehandle:
    for index in range(500, len(labels)):
        string = f'{images[index]}\t{labels[index]}\n'
        string = string.replace('\\', '/')
        string = string.replace('./', '')
        filehandle.write(string)