import os
import glob
import random

# read from images directory
#images_path = glob.glob('./images/*.png')
images_path = '/kaggle/working/data/gtavresize/images'


# split path and filename
images_path = [os.path.split(f) for f in images_path]

# get only the filename
images = [f[-1] for f in images_path]

# make random selection    
random.seed(42)
random.shuffle(images)


num_val_images = 2500
# write first X images to the file val.lst
with open('/kaggle/working/data/list/cityscapes_gtavresize/val.lst', 'w') as filehandle:
    for index in range(num_val_images):
        print(f'Index: {index}')
        string = f'images/{images[index]}\tlabels/{images[index]}\n'
        filehandle.write(string)

# write next X images to the file test.lst
with open('/kaggle/working/data/list/cityscapes_gtavresize/test.lst', 'w') as filehandle:
    for index in range(num_val_images, num_val_images*2):
        print(f'Index: {index}')
        string = f'images/{images[index]}\tlabels/{images[index]}\n'
        filehandle.write(string)

# write rest to the file train.lst
with open('/kaggle/working/data/list/cityscapes_gtavresize/train.lst', 'w') as filehandle:
    for index in range(num_val_images*2, len(images)):
        print(f'Index: {index}')
        string = f'images/{images[index]}\tlabels/{images[index]}\n'
        filehandle.write(string)